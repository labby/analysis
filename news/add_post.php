<?php

/**
 *  @module         news
 *  @version        see info.php of this module
 *  @author         Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos), LEPTON Project
 *  @copyright      2004-2010 Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos) 
 *  @copyright      2010-2022 LEPTON Project 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

global $section_id, $database, $page_id, $admin, $TEXT;

// Include admin wrapper script
require(LEPTON_PATH.'/modules/admin.php');

// Include the ordering class
// since 3.0.1 we use  LEPTON_order

// Get new order
$order = new LEPTON_order(TABLE_PREFIX.'mod_news_posts', 'position', 'post_id', 'section_id');
$position = $order->get_new($section_id);

// Get default commenting
$fetch_settings = array();
$database->execute_query(
	"SELECT commenting FROM ".TABLE_PREFIX."mod_news_settings WHERE section_id = '".$section_id."'",
	true,
	$fetch_settings,
	false
);
$commenting = $fetch_settings['commenting'];

// Insert new row into database
$fields = array(
	'section_id'	=> $section_id,
	'page_id'		=> $page_id,
	'position'		=> $position,
	'commenting'	=> $commenting,
	'published_when'=> time(),	// make sure that published_when has an entry that will be displayed in Frontend-View	
	'history_comment'	=> '',	//keep in mind, that text fields need a default value using INSERT
	'active'		=> 1,
	'link'			=> "",
	'content_short'	=> "",
	'content_long'	=> ""
);

$database->build_and_execute(
	"insert",
	TABLE_PREFIX."mod_news_posts",
	$fields
);

// Get the id
$post_id = $database->get_one("SELECT LAST_INSERT_ID() FROM ".TABLE_PREFIX."mod_news_posts");

// Say that a new record has been added, then redirect to modify page
if($database->is_error()) {
	$admin->print_error($database->get_error(), LEPTON_URL.'/modules/news/modify_post.php?page_id='.$page_id.'&section_id='.$section_id.'&post_id='.$post_id);
} else {
	?>
<script type="text/javascript">
		setTimeout("top.location.href ='<?php echo LEPTON_URL; ?>/modules/news/modify_post.php?page_id=<?php echo $page_id; ?>&section_id=<?php echo $section_id; ?>&post_id=<?php echo $post_id; ?>'", 0);
</script>
<?php
}

// Print admin footer
$admin->print_footer();
