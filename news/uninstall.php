<?php

/**
 *  @module         news
 *  @version        see info.php of this module
 *  @author         Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos), LEPTON Project
 *  @copyright      2004-2010 Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos) 
 *  @copyright      2010-2022 LEPTON Project 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

$database->simple_query("DELETE FROM ".TABLE_PREFIX."search WHERE name = 'module' AND value = 'news'");
$database->simple_query("DELETE FROM ".TABLE_PREFIX."search WHERE extra = 'news'");
$database->simple_query("DROP TABLE ".TABLE_PREFIX."mod_news_posts");
$database->simple_query("DROP TABLE ".TABLE_PREFIX."mod_news_groups");
$database->simple_query("DROP TABLE ".TABLE_PREFIX."mod_news_comments");
$database->simple_query("DROP TABLE ".TABLE_PREFIX."mod_news_settings");

require_once(LEPTON_PATH.'/framework/summary.functions.php');
rm_full_dir(LEPTON_PATH.PAGES_DIRECTORY.'/posts');
rm_full_dir(LEPTON_PATH.MEDIA_DIRECTORY.'/.news');
