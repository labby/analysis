<?php

/**
 *  @module         news
 *  @version        see info.php of this module
 *  @author         Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos), LEPTON Project
 *  @copyright      2004-2010 Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos) 
 *  @copyright      2010-2022 LEPTON Project 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */

// Check that GET values have been supplied
if(isset($_GET['page_id']) AND is_numeric($_GET['page_id'])) 
{
	$page_id = $_GET['page_id'];
} 
else 
{
	header('Location: /');
	exit(0);
}

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

if (isset($_GET['group_id']) AND is_numeric($_GET['group_id'])) {
	$group_id = (int)$_GET['group_id'];
} else {
	$group_id = -1;	// Keep in mind, that $group_id could be 0 (no group)
}
define('GROUP_ID', $group_id);

$oLEPTON = new LEPTON_frontend();
$oLEPTON->page_id = $page_id;
$oLEPTON->get_page_details();
$oLEPTON->get_website_settings();

/**
 *  Pre-check: any news on this page?
 */
$aAllModules = [];
$database->execute_query(
    "SELECT `module` FROM `".TABLE_PREFIX."sections` WHERE `page_id` = ".$page_id." AND `module`='news'",
    true,
    $aAllModules,
    true
);

if( 0 == count($aAllModules))
{
    die("No news traceable on this page!");
}

//checkout if a charset is defined otherwise use UTF-8
if(defined('DEFAULT_CHARSET')) {
	$charset=DEFAULT_CHARSET;
} else {
	$charset='utf-8';
}

ob_start();

// Sending XML header
header("Content-type: text/xml; charset=$charset" );

// Header info
// Required by CSS 2.0
echo '<?xml version="1.0" encoding="'.$charset.'"?>';
?> 
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
	<channel>
		<title><?php echo PAGE_TITLE; ?></title>
		<link>http://<?php echo $_SERVER['SERVER_NAME']; ?></link>
		<description> <?php echo PAGE_DESCRIPTION; ?></description>
<?php
	echo "<atom:link href='" . LEPTON_URL . "/modules/news/rss.php?page_id=$page_id' rel='alternate' type='application/rss+xml' />";
?>		
		<language><?php echo strtolower(DEFAULT_LANGUAGE); ?></language>
		<copyright><?php $thedate = date('Y'); echo "Copyright ".$thedate." by ".LEPTON_URL." , all rights reserved"; ?></copyright>
		<managingEditor><?php echo SERVER_EMAIL . " (" . MAILER_DEFAULT_SENDERNAME . ")"; ?></managingEditor>
		<webMaster><?php echo SERVER_EMAIL . " (" . MAILER_DEFAULT_SENDERNAME . ")"; ?></webMaster>
		<category><?php echo WEBSITE_TITLE; ?></category>
		<generator>LEPTON CMS, https://lepton-cms.org</generator>
<?php

// Get news items from database
$t = TIME();
$time_check_str= "(published_when = '0' OR published_when <= ".$t.") AND (published_until = 0 OR published_until >= ".$t.")";

//	Query
if ( $group_id > -1 ) {
	$query = "SELECT * FROM ".TABLE_PREFIX."mod_news_posts WHERE group_id=".$group_id." AND page_id = ".$page_id." AND active=1 AND ".$time_check_str." ORDER BY posted_when DESC";
} else {
	$query = "SELECT * FROM ".TABLE_PREFIX."mod_news_posts WHERE page_id=".$page_id." AND active=1 AND ".$time_check_str." ORDER BY posted_when DESC";	
}
$result = array();
$database->execute_query(
    $query,
    true,
    $result,
    true
);

// Generating the news items
foreach($result as $item)
{ 
	LEPTON_handle::restoreSpecialChars( $item['content_short'] );
?>
		<item>
			<title><![CDATA[<?php echo stripslashes($item["title"]); ?>]]></title>
			<description><![CDATA[<?php echo stripslashes($item["content_short"]); ?>]]></description>
			<guid><?php echo LEPTON_URL.PAGES_DIRECTORY.$item["link"].PAGE_EXTENSION; ?></guid>
			<link><?php echo LEPTON_URL.PAGES_DIRECTORY.$item["link"].PAGE_EXTENSION; ?></link>
			<pubDate><?php echo date(DATE_RSS,$item["published_when"]); ?></pubDate>
		</item>
<?php } ?>
	</channel>
</rss>

<?php  

$output = ob_get_clean();

$oLEPTON->preprocess($output);

// Load Droplet engine and process
if(file_exists(LEPTON_PATH .'/modules/droplets/droplets.php'))
{
    include_once LEPTON_PATH .'/modules/droplets/droplets.php';
    if(function_exists('evalDroplets'))
    {
        evalDroplets($output); 
    }
}

echo $output;
