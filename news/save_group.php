<?php

/**
 *  @module         news
 *  @version        see info.php of this module
 *  @author         Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos), LEPTON Project
 *  @copyright      2004-2010 Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos) 
 *  @copyright      2010-2022 LEPTON Project 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

// Include admin wrapper script
$update_when_modified = true; // Tells script to update when this page was last updated
require(LEPTON_PATH.'/modules/admin.php');

$aLookUpFields = [
    "group_id"  => ["type"  => "integer",       "default" => NULL ],
    "page_id"   => ["type"  => "integer",       "default" => NULL ],
    "title"     => ["type"  => "string_chars",  "default" => "" ],
    "active"    => ["type"  => "integer",       "default" => 0 ]
];

$aLookUpFields = LEPTON_request::getInstance()->testPostValues( $aLookUpFields );

//  Test for an invalid "group_id" or an invalid "page_id":
if( ( NULL === $aLookUpFields["group_id"] ) || ( NULL === $aLookUpFields["page_id"] ) )
{
    header("Location: ".ADMIN_URL."/pages/index.php");
	exit( 0 );
}

// We don't accept an empty title. 
if($aLookUpFields["title"] == "")
{
	$admin->print_error(
		$MESSAGE["GENERIC_FILL_IN_ALL"],
		LEPTON_URL."/modules/news/modify_group.php?page_id=".$aLookUpFields["page_id"]."&section_id=".$aLookUpFields["section_id"]."&group_id=".$aLookUpFields["group_id"]
	);
}

//  [Aldus: 2020-04-03] Temporär um den Rest vom Code nicht anfassen zu müssen
$title      = $aLookUpFields["title"];
$active     = $aLookUpFields["active"];
$page_id    = $aLookUpFields['page_id'];
$group_id   = $aLookUpFields['group_id'];

// Update row
$fields = array(
	'title' => $title,
	'active' => $active
);

$database->build_and_execute(
	'UPDATE',
	TABLE_PREFIX."mod_news_groups",
	$fields,
	"group_id = ".$group_id
);

// Check if the user uploaded an image or wants to delete one
if(isset($_FILES['image']['tmp_name']) AND $_FILES['image']['tmp_name'] != '')
{
	// Get real filename and set new filename
	$filename = $_FILES['image']['name'];
	
	// Aldus - 24.09.2019
	$aTempTerms = explode(".", $filename);
	$sMimeType = strtolower( array_pop($aTempTerms) );
	
	$new_filename = LEPTON_PATH.MEDIA_DIRECTORY.'/newspics/groups/image'.$group_id.'.'.$sMimeType;
	
	// Make sure the image is a jpg file
	
	$file4 = strtolower(substr($filename, -3));
	// $aTempTypes = ["jpg", "jpeg", "png", "gif"];
	
	if(!in_array($file4, news::IMAGE_FILE_TYPES)) // $aTempTypes))
    {
		$admin->print_error(
			$MESSAGE['GENERIC_FILE_TYPE'].' JPG (JPEG) or PNG  or GIF [1]',
			LEPTON_URL.'/modules/news/modify_group.php?page_id='.$page_id.'&section_id='.$section_id.'&group_id='.$group_id
		);
	}
	
	// Make sure the target directory exists
	LEPTON_handle::register("make_dir", "make_thumb");
	make_dir(LEPTON_PATH.MEDIA_DIRECTORY.'/newspics');
	make_dir(LEPTON_PATH.MEDIA_DIRECTORY.'/newspics/groups');
	
	// Upload image
	move_uploaded_file($_FILES['image']['tmp_name'], $new_filename);
	// Check if we need to create a thumb
	$resize = $database->get_one("SELECT `resize` FROM `".TABLE_PREFIX."mod_news_settings` WHERE `section_id` = ".$section_id );
	if($resize != 0)
    {
		// Resize the image
		$thumb_location = LEPTON_PATH.MEDIA_DIRECTORY.'/newspics/groups/thumb'.$group_id.'.'.$sMimeType;
		if(make_thumb($new_filename, $thumb_location, $resize))
        {
			// Delete the actual image and replace with the resized version
			unlink($new_filename);
			rename($thumb_location, $new_filename);
		}
	}
}
if(isset($_POST['delete_image']) AND $_POST['delete_image'] != '')
{
	
	$aTempList = news::IMAGE_FILE_TYPES; // ["jpg", "jpeg", "png", "gif"];
	foreach($aTempList as &$sFileType)
	{
	    // Try unlinking image
	    $sTempFilePath = LEPTON_PATH.MEDIA_DIRECTORY.'/newspics/groups/image'.$group_id.'.'.$sFileType;
	    
	    if(file_exists($sTempFilePath))
        {
		    unlink($sTempFilePath);
	    }
	}
}

// Check if there is a db error, otherwise say successful
if($database->is_error()) {
	$admin->print_error($database->get_error(), LEPTON_URL.'/modules/news/modify_group.php?page_id='.$page_id.'&section_id='.$section_id.'&group_id='.$group_id);
} else {
	$admin->print_success($TEXT['SUCCESS'], ADMIN_URL.'/pages/modify.php?page_id='.$page_id);
}
