<?php

/**
 *  @module         news
 *  @version        see info.php of this module
 *  @author         Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos), LEPTON Project
 *  @copyright      2004-2010 Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos) 
 *  @copyright      2010-2022 LEPTON Project 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

$oNP = news::getInstance();
$admin = LEPTON_admin::getInstance();

// use news class
if(isset($_POST['delete_version']) && is_numeric($_POST['delete_version']) )
{
	$to_delete = intval($_POST['delete_version']);
	$oNP->delete_history($to_delete);
	
	if(isset($_POST['show_history']))
	{
	    if($_POST['show_history'] == "")
	    {
	        unset($_POST['show_history']);
	    }
	}
    if(isset($_POST['show_draft']))
	{
	    if($_POST['show_draft'] == "")
	    {
	        unset($_POST['show_draft']);
	    }
	}
}

// Get id
if(!isset($_POST['show_history']) AND !isset($_POST['show_draft']))
{
	header("Location: ".ADMIN_URL."/pages/index.php");
	exit( 0 );
}
else
{
	$post_id = intval($_POST['show_history'] ?? $_POST['show_draft']);
}

$aAll_Histories = array();
$database->execute_query(
	"SELECT * FROM `".TABLE_PREFIX."mod_news_posts` WHERE history_post_id = '".$post_id."' AND history_type = 2",
	true,
	$aAll_Histories,
	true
);

$aDraft = array();
$database->execute_query(
	"SELECT * FROM `".TABLE_PREFIX."mod_news_posts` WHERE history_post_id = '".$post_id."' AND history_type = 1",
	true,
	$aDraft,
	false
);
//echo(LEPTON_tools::display($aDraft, 'pre','ui blue message'));

//	Additional values for history
foreach($aAll_Histories as &$ref)
{
    $ref['user_id_display_name'] = $database->get_one("SELECT `display_name` from `".TABLE_PREFIX."users` WHERE `user_id`=". $ref['history_user']);
    $ref['user_id_hist_display_name'] = $ref['user_id_display_name']; // Prüfen
}
unset($ref);

if(empty($aDraft))
{
//	Additional values for draft
    $aDraft['user_id_display_name'] = $database->get_one("SELECT `display_name` from `".TABLE_PREFIX."users` WHERE `user_id`=". $aDraft['history_user']);
    $aDraft['user_id_hist_display_name'] = $aDraft['user_id_display_name']; // Prüfen
}

$form_values = array(
	'oNP'           => $oNP,
	'user_id'       => $_SESSION['USER_ID'],
	'page_id'       => $page_id,
	'section_id'    => $section_id,
	'post_id'       => $post_id,
	'all_histories' => $aAll_Histories,
	'draft' 		=> $aDraft,
	'leptoken'		=> get_leptoken(),
	'show_history'  => ($_POST['show_history'] ?? ""),
	'show_draft'    => ($_POST['show_draft'] ?? "")
);

if(isset($_POST['show_history']))
{
	$oTWIG = lib_twig_box::getInstance();
	$oTWIG->registerModule("news");

	echo $oTWIG->render(
		'@news/history.lte',
		$form_values
	);	
}
else
{
	$oTWIG = lib_twig_box::getInstance();
	$oTWIG->registerModule("news");

	echo $oTWIG->render(
		'@news/draft.lte',
		$form_values
	);	
}


// Print admin footer
$admin->print_footer();
