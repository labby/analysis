<?php

/**
 *  @module         news
 *  @version        see info.php of this module
 *  @author         Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos), LEPTON Project
 *  @copyright      2004-2010 Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos) 
 *  @copyright      2010-2022 LEPTON Project 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

//  [1]
$table_fields="
    `comment_id` INT NOT NULL AUTO_INCREMENT,
    `section_id` INT NOT NULL DEFAULT '0',
    `page_id` INT NOT NULL DEFAULT '0',
	`post_id` INT NOT NULL DEFAULT '0',
	`title` VARCHAR(255) NOT NULL DEFAULT '',
	`comment` TEXT NOT NULL ,
	`commented_when` INT NOT NULL DEFAULT '0',
	`commented_by` INT NOT NULL DEFAULT '0',
    PRIMARY KEY (`comment_id`)
";
LEPTON_handle::install_table('mod_news_comments', $table_fields);

//  [2]
$table_fields="
    `group_id` INT NOT NULL AUTO_INCREMENT,
    `section_id` INT NOT NULL DEFAULT '0',
    `page_id` INT NOT NULL DEFAULT '0',
	`active` INT NOT NULL DEFAULT '0',
	`position` INT NOT NULL DEFAULT '0',
	`title` VARCHAR(255) NOT NULL DEFAULT '',
    PRIMARY KEY (`group_id`)
";
LEPTON_handle::install_table('mod_news_groups', $table_fields);	

//  [3]
$table_fields="
    `post_id` INT NOT NULL AUTO_INCREMENT,
    `section_id` INT NOT NULL DEFAULT '0',
    `page_id` INT NOT NULL DEFAULT '0',
    `group_id` INT NOT NULL DEFAULT '0',
	`active` INT NOT NULL DEFAULT '0',
	`position` INT NOT NULL DEFAULT '0',
	`title` VARCHAR(255) NOT NULL DEFAULT '',
	`link` TEXT NOT NULL ,
	`content_short` TEXT NOT NULL ,
	`content_long` TEXT NOT NULL ,
	`commenting` VARCHAR(7) NOT NULL DEFAULT '',
	`published_when` INT NOT NULL DEFAULT '0',
	`published_until` INT NOT NULL DEFAULT '0',
	`posted_when` INT NOT NULL DEFAULT '0',
	`posted_by` INT NOT NULL DEFAULT '0',
    `history_post_id` int(11) DEFAULT -1,
    `history_max` int(1) NOT NULL DEFAULT -1,
    `history_user` int(1) NOT NULL DEFAULT -1,
    `history_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ,
	`history_type` int(1) NOT NULL DEFAULT -1,
    `history_comment` text NOT NULL,
    PRIMARY KEY (`post_id`)
";
LEPTON_handle::install_table('mod_news_posts', $table_fields);		

//  [4]
$table_fields="
    `section_id` INT NOT NULL DEFAULT '0',
    `page_id` INT NOT NULL DEFAULT '0',
	`posts_per_page` INT NOT NULL DEFAULT 5,
	`commenting` VARCHAR(7) NOT NULL DEFAULT '',
	`resize` INT NOT NULL DEFAULT '0',
	`use_captcha` INT NOT NULL DEFAULT '0',
    PRIMARY KEY (`section_id`)
";
LEPTON_handle::install_table('mod_news_settings', $table_fields);		

//  [5]
$aTestSearchEntries = array();
$database->execute_query(
    "SELECT * FROM ".TABLE_PREFIX."search  WHERE value = 'news'",
    true,
    $aTestSearchEntries,
    true
);
	
if( 0 === count($aTestSearchEntries) )
{
    // Insert info into the search table
    // Module query info
    $field_info = array();
    $field_info['page_id'] = 'page_id';
    $field_info['title'] = 'page_title';
    $field_info['link'] = 'link';
    $field_info['description'] = 'description';
    $field_info['modified_when'] = 'modified_when';
    $field_info['modified_by'] = 'modified_by';
    $field_info = serialize($field_info);	

	// Query start
	$query_start_code = "SELECT [TP]pages.page_id, [TP]pages.page_title,	[TP]pages.link, [TP]pages.description, [TP]pages.modified_when, [TP]pages.modified_by	FROM [TP]mod_news_posts, [TP]mod_news_groups, [TP]mod_news_comments, [TP]mod_news_settings, [TP]pages WHERE ";
    $query_body_code = "
    	[TP]pages.page_id = [TP]mod_news_posts.page_id AND [TP]mod_news_posts.title LIKE \'%[STRING]%\'
    	OR [TP]pages.page_id = [TP]mod_news_posts.page_id AND [TP]mod_news_posts.content_short LIKE \'%[STRING]%\'
    	OR [TP]pages.page_id = [TP]mod_news_posts.page_id AND [TP]mod_news_posts.content_long LIKE \'%[STRING]%\'
    	OR [TP]pages.page_id = [TP]mod_news_comments.page_id AND [TP]mod_news_comments.title LIKE \'%[STRING]%\'
    	OR [TP]pages.page_id = [TP]mod_news_comments.page_id AND [TP]mod_news_comments.comment LIKE \'%[STRING]%\'
    	OR [TP]pages.page_id = [TP]mod_news_settings.page_id AND [TP]mod_news_settings.header LIKE \'%[STRING]%\'
    	OR [TP]pages.page_id = [TP]mod_news_settings.page_id AND [TP]mod_news_settings.footer LIKE \'%[STRING]%\'
    	OR [TP]pages.page_id = [TP]mod_news_settings.page_id AND [TP]mod_news_settings.post_header LIKE \'%[STRING]%\'
    	OR [TP]pages.page_id = [TP]mod_news_settings.page_id AND [TP]mod_news_settings.post_footer LIKE \'%[STRING]%\'
    	OR [TP]pages.page_id = [TP]mod_news_settings.page_id AND [TP]mod_news_settings.comments_header LIKE \'%[STRING]%\'
    	OR [TP]pages.page_id = [TP]mod_news_settings.page_id AND [TP]mod_news_settings.comments_footer LIKE \'%[STRING]%\'
	";	
	$query_end_code = "";	
	
	$field_values="
        (NULL,'module', 'news', '".$field_info."'),	
        (NULL, 'query_start', '".$query_start_code."', 'news'),
        (NULL,'query_body', '".$query_body_code."', 'news'),
        (NULL,'query_end', '".$query_end_code."', 'news')
	";

	LEPTON_handle::insert_values('search', $field_values);	
}

//  [6]
//  Make news post access files dir
require_once(LEPTON_PATH.'/framework/summary.functions.php');
make_dir(LEPTON_PATH.MEDIA_DIRECTORY.'/newspics'); // create directory for images
	
if(make_dir(LEPTON_PATH.PAGES_DIRECTORY.'/posts')) 
{
	// Add a index.php file to prevent directory spoofing
	$content = ''.
"<?php

/**
 *  @module         news
 *  @version        see info.php of this module
 *  @author         Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos), LEPTON Project
 *  @copyright      2004-2010 Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos) 
 *  @copyright      2010-2022 LEPTON Project 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */

header('Location: ../');
?>";
	
	$handle = fopen(LEPTON_PATH.PAGES_DIRECTORY.'/posts/index.php', 'w');
	fwrite($handle, $content);
	fclose($handle);
	change_mode(LEPTON_PATH.PAGES_DIRECTORY.'/posts/index.php', 'file');

    //  [6.2]		
	/**
	 *	Try to copy the index.php also in the newspics folder inside
	 *	the media-directory.
	 *
	 */
	copy(
		LEPTON_PATH.PAGES_DIRECTORY.'/posts/index.php',
		LEPTON_PATH.MEDIA_DIRECTORY.'/newspics/index.php'
	);
}
