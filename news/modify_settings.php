<?php

/**
 *  @module         news
 *  @version        see info.php of this module
 *  @author         Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos), LEPTON Project
 *  @copyright      2004-2010 Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos) 
 *  @copyright      2010-2022 LEPTON Project 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

$oNEWS = news::getInstance();
global $database;

// Include admin wrapper script
require(LEPTON_PATH.'/modules/admin.php');

// include core functions of WB 2.7 to edit the optional module CSS files (frontend.css, backend.css)
include_once(LEPTON_PATH .'/framework/summary.module_edit_css.php');


// Get settings from the DB
$fetch_content = array();
$database->execute_query(
	"SELECT * FROM `".TABLE_PREFIX."mod_news_settings` WHERE `section_id` = '".$section_id."'",
	true,
	$fetch_content,
	false
);

ob_start();
   edit_module_css('news');
   $called_edit_module_css = ob_get_clean();
    
/*	**********
 *	Here we go 
 */
$form_values = array(
	'LEPTON_PATH' => LEPTON_PATH,
	'LEPTON_URL' => LEPTON_URL,
	'ADMIN_URL' => ADMIN_URL,
	'TEXT'	=> $TEXT,
	'HEADING' => $HEADING,
	'MOD_NEWS'	=> $oNEWS->language,
	'leptoken' => (isset($_GET['leptoken']) ? $_GET['leptoken'] : ""),
	'page_id'	=> $page_id,
	'section_id'	=> $section_id,
	'posts_per_page' => $fetch_content['posts_per_page'],
	'extension_loaded_gd' => extension_loaded('gd') ? 1 : 0,
	'imageCreateFromJpeg' => function_exists('imageCreateFromJpeg') ? 1 : 0,
	'commenting' => $fetch_content['commenting'],
	'use_captcha' => $fetch_content['use_captcha'],
	'resize' => $fetch_content['resize'],
	'edit_module_css' => $called_edit_module_css
);

$oTWIG = lib_twig_box::getInstance();	
$oTWIG->registerModule("news");

echo $oTWIG->render(
	'@news/modify_settings.lte',
	$form_values
);

// Print admin footer
$admin->print_footer();

?>