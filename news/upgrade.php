<?php

/**
 *  @module         news
 *  @version        see info.php of this module
 *  @author         Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos), LEPTON Project
 *  @copyright      2004-2010 Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos) 
 *  @copyright      2010-2022 LEPTON Project 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

//  [1]
$sLookForFolder = LEPTON_PATH.MEDIA_DIRECTORY."/.news";
if(file_exists($sLookForFolder))
{
    LEPTON_handle::register( "make_dir", "rename_recursive_dirs" );
    
    make_dir(LEPTON_PATH.MEDIA_DIRECTORY.'/newspics');
	make_dir(LEPTON_PATH.MEDIA_DIRECTORY.'/newspics/groups');
	
    rename_recursive_dirs( $sLookForFolder, LEPTON_PATH.MEDIA_DIRECTORY."/newspics/groups/" );
}

//  [2]
//  delete obsolete file
$file_names = array(
	'/modules/news/languages/NO.php',
	'/modules/news/languages/SE.php',
	'/modules/news/register_language.php',
	'/modules/news/register_parser.php'
);
LEPTON_handle::delete_obsolete_files( $file_names );
LEPTON_handle::delete_obsolete_files( MEDIA_DIRECTORY."/.news" );

//  [2.0.1]
$database = LEPTON_database::getInstance();

//  [2.1]
//  make sure to get a default value
$database->execute_query("ALTER TABLE `".TABLE_PREFIX."mod_news_settings` CHANGE `commenting` `commenting` VARCHAR(7) NOT NULL DEFAULT '' ");
$database->execute_query("ALTER TABLE `".TABLE_PREFIX."mod_news_settings` CHANGE `posts_per_page` `posts_per_page` INT(11) NOT NULL DEFAULT 5 ");


//  [3]
$aLookForFields = [
    "history_post_id" => "int(11) DEFAULT -1 ",
    "history_max"     => "int(1) NOT NULL DEFAULT -1 ",
    "history_user"    => "int(11) NOT NULL DEFAULT -1 ",	
    "history_time"    => "timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ",
    "history_type"    => "int(1) NOT NULL DEFAULT -1 ",
    "history_comment" => "text NOT NULL"
];

$aTableInfo = [];
$database->describe_table(
    TABLE_PREFIX."mod_news_posts",
    $aTableInfo,
    LEPTON_database::DESCRIBE_ONLY_NAMES
);

//die(LEPTON_tools::display($aTableInfo, 'pre','ui blue message'));

$aLookForNames = array_keys($aLookForFields);
foreach($aLookForNames as $ref){
    if(in_array($ref,$aTableInfo))
    {
        $aLookForFields[ $ref ] = '';
    }
}

foreach($aLookForFields as $aTempName => $sOptions)
{
    if($sOptions != '')
    {
        $database->simple_query("ALTER TABLE `".TABLE_PREFIX."mod_news_posts` ADD `".$aTempName."` ".$sOptions);
    }
}
