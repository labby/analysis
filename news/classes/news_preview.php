<?php

/**
 *  @module         news
 *  @version        see info.php of this module
 *  @author         Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos), LEPTON Project
 *  @copyright      2004-2010 Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos) 
 *  @copyright      2010-2022 LEPTON Project 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */
 
 
class news_preview
{
    static $instance;
    
    const HISTORY_TABLE = TABLE_PREFIX."mod_news_posts";
    
    static $iUsePreview = false;
    
    static $iPreviewID = 0;

    static $aPreviewData = [];
    
    static function getPreviewData( $iID = 0 )
    {
        $database = LEPTON_database::getInstance();
      
        self::$aPreviewData = [];
        $database->execute_query(
            "SELECT * FROM `".SELF::HISTORY_TABLE."` WHERE `post_id` = ".$iID,
            true,
            self::$aPreviewData,
            false
        );
        //die(LEPTON_tools::display( self::$aPreviewData ));
        if( 0 < count(self::$aPreviewData))
        {
            self::$iUsePreview = true;
            self::$iPreviewID = $iID; 
        
            $aTempLookUp = ["content_short", "content_long", "title"];
            foreach($aTempLookUp as $ref)
            {
                self::$aPreviewData[ $ref ] = htmlspecialchars_decode( self::$aPreviewData[ $ref ] );
            }
                
            $aPageData = [];
            $database->execute_query("
                SELECT `section_id`,`page_id` FROM `".SELF::HISTORY_TABLE."` WHERE `post_id` = ".self::$aPreviewData['history_post_id'],
                true,
                $aPageData,
                false
            );
            
            if( 0 < count($aPageData) )
            {
                self::$aPreviewData['section_id'] = $aPageData['section_id'];
                self::$aPreviewData['page_id'] = $aPageData['page_id'];
            }
            
            return true;        
        
        } else {
            return false;
        }
    }
}