<?php

/**
 *  @module         news
 *  @version        see info.php of this module
 *  @author         Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos), LEPTON Project
 *  @copyright      2004-2010 Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos) 
 *  @copyright      2010-2022 LEPTON Project 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */

class news extends LEPTON_abstract
{
	const IMAGE_FILE_TYPES = ["jpg", "jpeg", "png", "gif"];
	
	static $instance;
    
    public $display_details = false;
    
    public $displayed_news = 0;
    
    public $allGroups = array();
	
	public $action_url = LEPTON_URL.'/modules/news/';
    
    public function initialize()
    {
        self::$instance->getAllGroups();
    }
    
    private function getAllGroups()
    {
        $aTemp = array();
        LEPTON_database::getInstance()->execute_query(
            "SELECT * FROM `".TABLE_PREFIX."mod_news_groups`",
            true,
            $aTemp,
            true
        );
        
        foreach($aTemp as $group)
        {
            self::$instance->allGroups[ $group['group_id'] ] = $group;
        }
    }
    
    public function getHeadInfo()
    {
        $aReturnVars = array(
            "title" => "",
            "description" => "",
            "keywords"  => ""
        );
        if( (defined("POST_ID")) && (is_numeric(POST_ID)))
        {
            $aInfo = array();
            LEPTON_database::getInstance()->execute_query(
                "SELECT `title`,`content_short`,`content_long` FROM `".TABLE_PREFIX."mod_news_posts` WHERE `post_id`=".POST_ID,
                true,
                $aInfo,
                false
            );
            if(count($aInfo) > 0)
            {
                $aReturnVars["title"] = $aInfo["title"];
                $aReturnVars["description"] = $aInfo["content_short"];
                $aReturnVars["keywords"]    = "Test,".$aInfo["title"];
            }
        } else {
            // News übersichtseite ...
            $aAllNewsOnThisPage = array();
            LEPTON_database::getinstance()->execute_query(
                "SELECT `title`,`content_short` FROM `".TABLE_PREFIX."mod_news_posts` WHERE `page_id`=".PAGE_ID,
                true,
                $aAllNewsOnThisPage,
                true
            );
            $aReturnVars["description"] = "News for: ";
            foreach($aAllNewsOnThisPage as $aTempNews)
            {
                $aReturnVars["description"] .= "\n- ".$aTempNews["title"];
            }
        }
        return $aReturnVars;
    }
	
    public function delete_history( $id )
	{
		LEPTON_database::getInstance()->simple_query("DELETE FROM ".TABLE_PREFIX."mod_news_posts WHERE post_id = ".$id );

	}
	
	static function cleanUpString( &$sAnyText )
	{
	    $sAnyText = str_replace(
	        ["&amp;"],
	        ["&"],
	        $sAnyText
	    );
	}	
}
