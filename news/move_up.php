<?php

/**
 *  @module         news
 *  @version        see info.php of this module
 *  @author         Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos), LEPTON Project
 *  @copyright      2004-2010 Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos) 
 *  @copyright      2010-2022 LEPTON Project 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

// Get id
if(!isset($_GET['post_id']) OR !is_numeric($_GET['post_id'])) {
	if(!isset($_GET['group_id']) OR !is_numeric($_GET['group_id']))
	{
        header("Location: index.php");
        exit( 0 );
	} else {
		$id = $_GET['group_id'];
		$id_field = 'group_id';
		$table = TABLE_PREFIX.'mod_news_groups';
	}
} else {
	$id = $_GET['post_id'];
	$id_field = 'post_id';
	$table = TABLE_PREFIX.'mod_news_posts';
}

// Include admin wrapper script
require(LEPTON_PATH.'/modules/admin.php');

/**
 *  Aldus [2021-10-08]
 *  New in L* 5.3 - backstep because LEPTON_order does't work here as aspected!
 */
$database = LEPTON_database::getInstance();
$aActualPost = [];
$database->execute_query(
    "SELECT `position`,`section_id`,`post_id`
        FROM `".TABLE_PREFIX."mod_news_posts`
        WHERE `post_id` = ".$id,
    true,
    $aActualPost,
    false
);

// get the hightes entry in this section
$iLastPosition = $database->get_one(
    "SELECT `position` FROM `".TABLE_PREFIX."mod_news_posts` WHERE `section_id` = ".$aActualPost['section_id']. " ORDER BY `position` DESC LIMIT 1"
);

if(intval($aActualPost['position']) < intval($iLastPosition))
{
    // [2.1] next entry
    $aNextEntry = [];
    $database->execute_query(
        "SELECT `post_id`,`position` 
            FROM `".TABLE_PREFIX."mod_news_posts` 
            WHERE `section_id`=".$aActualPost['section_id']." 
            AND `position` > ".$aActualPost['position']." 
            ORDER BY `position` ASC",
        true,
        $aNextEntry,
        false
    );
    
    //  [2.1.1] valid entry?
    if(!empty($aNextEntry))
    {
        //  [2.2] Swap positions
        //  [2.2.1]
        $database->simple_query(
            "UPDATE `".TABLE_PREFIX."mod_news_posts` set `position` = ".$aNextEntry['position']." WHERE `post_id` = ".$aActualPost['post_id']
        );
        //  [2.2.2]
        $database->simple_query(
            "UPDATE `".TABLE_PREFIX."mod_news_posts` set `position` = ".$aActualPost['position']." WHERE `post_id` = ".$aNextEntry['post_id']
        );
    }
}

if($database->is_error() === false)
{
	$admin->print_success($TEXT['SUCCESS'], ADMIN_URL.'/pages/modify.php?page_id='.$page_id);
} else {
	$admin->print_error($TEXT['ERROR']." [1]\n".$order->errorMessage, ADMIN_URL.'/pages/modify.php?page_id='.$page_id);
}
