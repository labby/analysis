<?php

/**
 *  @module         news
 *  @version        see info.php of this module
 *  @author         Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos), LEPTON Project
 *  @copyright      2004-2010 Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos) 
 *  @copyright      2010-2022 LEPTON Project 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

// Get id
if(!isset($_GET['post_id']) OR !is_numeric($_GET['post_id'])) {
	header("Location: ".ADMIN_URL."/pages/index.php");
	exit(0);
} else {
	$post_id = $_GET['post_id'];
}

// Include admin wrapper script
require(LEPTON_PATH.'/modules/admin.php');
$oNP = news::getInstance();
// Get header and footer
$current_content = array();
$query_content = $database->execute_query(
	"SELECT * FROM `".TABLE_PREFIX."mod_news_posts` WHERE `post_id` = '".$post_id."'",
	true,
	$current_content,
	false
);

//  aldus - 2021-06-18
news::cleanUpString( $current_content['title'] );

if (!defined('WYSIWYG_EDITOR') OR WYSIWYG_EDITOR=="none" OR !file_exists(LEPTON_PATH.'/modules/'.WYSIWYG_EDITOR.'/include.php')) {
	function show_wysiwyg_editor($name,$id,$content,$width,$height, $promp) {
		$sHTMLCode = '<textarea name="'.$name.'" id="'.$id.'" rows="10" cols="1" style="width: '.$width.'; height: '.$height.';">'.$content.'</textarea>';
		if(true === $promp)
		{
		    echo $sHTMLCode;
		} else {
		    return $sHTMLCode;
		}
	}
} else {
	$id_list=array("short","long");
	require(LEPTON_PATH.'/modules/'.WYSIWYG_EDITOR.'/include.php');
}

/**
 * Use images? Since version 3.7.0 for LEPTON-CMS we're always using images!
*/
$use_images = TRUE;

/**
 * Get the groups for this page/section
 */
$groups = array();
$query = $database->execute_query(
	"SELECT `group_id`,`title` FROM `".TABLE_PREFIX."mod_news_groups` WHERE `section_id` = '".$section_id."' ORDER BY `position` ASC",
	true,
	$groups
);

foreach($groups as &$ref) {
	$ref['selected'] = ($ref['group_id'] == $current_content['group_id'])
		? "selected='selected'"
		: ""
		;
}
/**
 *	Get the comments for this post
 */
$comments = array();
$query_comments = $database->execute_query(
	"SELECT * FROM `".TABLE_PREFIX."mod_news_comments` WHERE `section_id` = '".$section_id."' AND `post_id` = '".$post_id."' ORDER BY commented_when DESC",
	true,
	$comments
);

/**	*******************************
 *	Here we go ....
 */

$bFileExists = false;
$aMimetypes = ["jpg", "jpeg", "png", "gif"];
$sImageFilePath = "";
foreach($aMimetypes as &$sFiletype)
{
    $sImageFilePath = MEDIA_DIRECTORY.'/newspics/image'.$post_id.'.'.$sFiletype;
    if(file_exists(LEPTON_PATH.$sImageFilePath))
    {
        $bFileExists = true;
        $sImageFilePath = LEPTON_URL.$sImageFilePath;
        break;
    }
}

//  Get user date format here.
$sTempDateFormat = $database->get_one("SELECT `date_format` FROM `".TABLE_PREFIX."users` WHERE `user_id`=".$_SESSION["USER_ID"]);
if( ( NULL != $sTempDateFormat ) && (strlen($sTempDateFormat) > 0) )
{
    // keep it as it is

} 
else 
{
    // use the currend date_format setting
    $sTempDateFormat = DATE_FORMAT;
}

// check for WC
$wc_exist = $database->get_one("SELECT `post_id` FROM `".TABLE_PREFIX."mod_news_posts` WHERE (`history_type` = 1) AND (`history_post_id` = ".$post_id.")");

$form_values = array(
    'MEDIA_DIRECTORY'   => MEDIA_DIRECTORY,
	'WYSIWYG_HISTORY'   => WYSIWYG_HISTORY,
	'DEFAULT_LANGUAGE'  => DEFAULT_LANGUAGE,
	'oNP' 				=> $oNP,
	'user_id'           => $_SESSION['USER_ID'],
    'page_id'           => $page_id,
    'section_id'        => $section_id,
    'post_id'           => $post_id,
	'wc_exist'          => $wc_exist,
    'link'              => $current_content['link'],
    'title'             => $current_content['title'],
    'groups'            => $groups,
    'commenting'        => $current_content['commenting'],
    'active'            => lib_fomantic::BuildCheckbox(
            "active",   // name
            "active",   // html-id
            $TEXT["ACTIVE"], // label
            1,          // value
            $current_content['active'],   // state
            ""          // css class?
        ),
    'published_when'    => $current_content['published_when'],
    'published_until'   => $current_content['published_until'],

    'use_images'        => $use_images,

    'got_image'         => ($bFileExists == true) ? 1 : 0,
    'image_file_url'    => $sImageFilePath,
    
    'show_wysiwyg_editor_short' => show_wysiwyg_editor('short','short', $current_content['content_short'],'100','250px', false),
    'show_wysiwyg_editor_long'  => show_wysiwyg_editor('long','long', $current_content['content_long'],'100','550px', false),    
    'num_of_comments'   => count($comments),
    'comments'          => $comments,
    'row'               => 'a',
//    'TEXT'              => $TEXT, // !
    'DATE_FORMAT'       => $sTempDateFormat,
    'DATEPICKER_FORMAT' => lib_lepton::getToolInstance("datetools")->formatToDatepicker( $sTempDateFormat ),
    'leptoken'          => get_leptoken()
);

$_SESSION['last_edit_section'] = $section_id;

$oTWIG = lib_twig_box::getInstance();
$oTWIG->registerModule("news");

echo $oTWIG->render(
	'@news/modify_post.lte',
	$form_values
);

// Print admin footer
$admin->print_footer();
