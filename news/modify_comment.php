<?php

/**
 *  @module         news
 *  @version        see info.php of this module
 *  @author         Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos), LEPTON Project
 *  @copyright      2004-2010 Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos) 
 *  @copyright      2010-2022 LEPTON Project 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

// Get id
if(!isset($_GET['comment_id']) OR !is_numeric($_GET['comment_id'])) {
	header("Location: ".ADMIN_URL."/pages/index.php");
	exit(0);
} else {
	$comment_id = $_GET['comment_id'];
}

// Include admin wrapper script
require(LEPTON_PATH.'/modules/admin.php');

// Get header and footer
$fetch_content = array();
$query_content = $database->execute_query(
	"SELECT `post_id`,`title`,`comment` FROM `".TABLE_PREFIX."mod_news_comments` WHERE comment_id = ".$comment_id."",
	true,
	$fetch_content,
	false
);

$form_values = array(
	'TEXT_MODIFY'	=> $TEXT['MODIFY'],
	'LEPTON_URL'	=> LEPTON_URL,
	'section_id'	=> $section_id,
	'page_id'		=> $page_id,
	'post_id'		=> $fetch_content['post_id'],
	'comment_id'	=> $comment_id,
	'TEXT_TITLE'	=> $TEXT['TITLE'],
	'title'			=> $fetch_content['title'],
	'TEXT_COMMENT'	=> $TEXT['COMMENT'],
	'comment'		=> $fetch_content['comment'],
	'SAVE'			=> $TEXT['SAVE'],
	'CANCEL'		=> $TEXT['CANCEL']
);

$oTWIG = lib_twig_box::getInstance();
$oTWIG->registerModule("news");

echo $oTWIG->render(
	'@news/modify_comment.lte',
	$form_values
);

// Print admin footer
$admin->print_footer();
