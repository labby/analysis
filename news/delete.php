<?php

/**
 *  @module         news
 *  @version        see info.php of this module
 *  @author         Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos), LEPTON Project
 *  @copyright      2004-2010 Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos) 
 *  @copyright      2010-2022 LEPTON Project 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

// get and remove all php files created for the news section
$query_details = array();
$database->execute_query(
	"SELECT `link` FROM `".TABLE_PREFIX."mod_news_posts` WHERE `section_id` = '".$section_id."'",
	true,
	$query_details
);

foreach($query_details as $link) {
	if(is_writable(LEPTON_PATH.PAGES_DIRECTORY.$link['link'].PAGE_EXTENSION)) {
		unlink(LEPTON_PATH.PAGES_DIRECTORY.$link['link'].PAGE_EXTENSION);
	}
}

$database->execute_query("DELETE FROM `".TABLE_PREFIX."mod_news_posts` WHERE `section_id` = '".$section_id."'");
$database->execute_query("DELETE FROM `".TABLE_PREFIX."mod_news_groups` WHERE `section_id` = '".$section_id."'");
$database->execute_query("DELETE FROM `".TABLE_PREFIX."mod_news_comments` WHERE `section_id` = '".$section_id."'");
$database->execute_query("DELETE FROM `".TABLE_PREFIX."mod_news_settings` WHERE `section_id` = '".$section_id."'");

?>