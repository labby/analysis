<?php

/**
 *  @module         news
 *  @version        see info.php of this module
 *  @author         Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos), LEPTON Project
 *  @copyright      2004-2010 Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos) 
 *  @copyright      2010-2022 LEPTON Project 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

//	Modul Description
$module_description = 'Ten moduł wyświetla stronę wiadomości (News).';

$MOD_NEWS = array (
	//	Variables for the backend
	'SETTINGS' => 'News Ustawienia',
	'CONFIRM_DELETE'	=> 'Jestes pewien usuniecia &quot;%s&quot;?',
	
	//	Variables for the frontend
	'TEXT_READ_MORE' => 'Czytaj więcej',
	'TEXT_POSTED_BY' => 'Napisał(a)',
	'TEXT_ON' => 'dnia',
	'TEXT_LAST_CHANGED' => 'Edytowano',
	'TEXT_AT' => 'o',
	'TEXT_BACK' => 'Wstecz',
	'TEXT_COMMENTS' => 'Komentarze',
	'TEXT_COMMENT' => 'Komentarz',
	'TEXT_ADD_COMMENT' => 'Dodaj komentarz',
	'TEXT_BY' => 'Dodano: ',
	'TEXT_PAGE_NOT_FOUND' => 'Strona nie istnieje',
	'TEXT_UNKNOWN' => 'Gość',
	'TEXT_NO_COMMENT' => 'none available',
	
	//	Variables for History
	'action'		=> 'Action',
	'autosave_always'	=> 'Publish changes and keep old content (max. '.MAX_WYSIWYG_HISTORY.')',
	'button_copy'	=> 'Drafts',
	'button_history'=> 'History',
	'comment'		=> 'Comment',
	'date'			=> 'Date',
	'docs'			=> 'Documentation',
	'header1'		=> 'No',
	'publish_changes'=> 'Publish changes and replace old content (default)',
	'pushed_by'		=> 'Pushed to history by',
	'saved_by'		=> 'Saved by',
	'save_ok'		=> 'Data saved successfull!',
	'use_workingcopy'=> 'Save as draft (only one draft per section possible)',
	'version_delete'=> 'Delete Version',
	'version_restore'=> 'Restore Version',
	'version_view'=> 'Preview Version',
	'want_really'	=> "really",	
	'want_delete'	=> "Do you want to delete version from ",
	'want_delete_wc'=> "Do you want to delete draft of section ",
	'wcopy_by'		=> 'Saved as draft by',	
	'wc_delete'		=> 'Delete draft',
	'wc_preview'	=> 'Preview draft',	
	'wc_restore'	=> 'Restore draft',	
	'what_to_do'	=> 'Please choose',
	
    // own date formats
    // see: https://www.php.net/manual/de/intldateformatter.format.php
    //      especially the examples
    // see: https://unicode-org.github.io/icu/userguide/format_parse/datetime/#formatting-dates
    'own_date_full_format'      => "EEEE, dd. MMMM yyyy - HH:mm" // "EEEE', the' dd. MMMM yyyy 'at' HH:mm 'o´clock' zz"

);