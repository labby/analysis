<?php

/**
 *  @module         news
 *  @version        see info.php of this module
 *  @author         Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos), LEPTON Project
 *  @copyright      2004-2010 Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos) 
 *  @copyright      2010-2022 LEPTON Project 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

//	Modul Description
$module_description = 'This page type is designed for making a news page.';

$MOD_NEWS = array(
	//	Variables for the backend
	'SETTINGS' => 'News Settings',
	'CONFIRM_DELETE'	=> 'Are you sure you want to delete the news-text \n&quot;%s&quot;?',
	
	//	Variables for the frontend
	'TEXT_READ_MORE' => 'Read More',
	'TEXT_POSTED_BY' => 'Posted by',
	'TEXT_ON' => 'on',
	'TEXT_LAST_CHANGED' => 'Last changed',
	'TEXT_AT' => 'at',
	'TEXT_BACK' => 'Back',
	'TEXT_COMMENTS' => 'Comments',
	'TEXT_COMMENT' => 'Comment',
	'TEXT_ADD_COMMENT' => 'Add Comment',
	'TEXT_BY' => 'By',
	'TEXT_PAGE_NOT_FOUND' => 'Page not found',
	'TEXT_UNKNOWN' => 'Guest',
	'TEXT_NO_COMMENT' => 'none available',
	
	//	Variables for History
	'action'		=> 'Action',
	'autosave_always'	=> 'Publish changes and keep old content (max. '.MAX_WYSIWYG_HISTORY.')',
	'button_copy'	=> 'Drafts',
	'button_history'=> 'History',
	'comment'		=> 'Comment',
	'date'			=> 'Date',
	'docs'			=> 'Documentation',
	'header1'		=> 'No',
	'publish_changes'=> 'Publish changes and replace old content (default)',
	'pushed_by'		=> 'Pushed to history by',
	'saved_by'		=> 'Saved by',
	'save_ok'		=> 'Data saved successfull!',
	'use_workingcopy'=> 'Save as draft (only one draft per section possible)',
	'version_delete'=> 'Delete version',
	'version_restore'=> 'Restore version',
	'version_view'=> 'Preview version',
	'want_really'	=> "really",	
	'want_delete'	=> "Do you want to delete version from ",
	'want_delete_wc'=> "Do you want to delete draft of section ",
	'wcopy_by'		=> 'Saved as draft by',	
	'wc_delete'		=> 'Delete draft',
	'wc_preview'	=> 'Preview draft',	
	'wc_restore'	=> 'Restore draft',	
	'what_to_do'	=> 'Please choose',
        
    // own date formats
    // see: https://www.php.net/manual/de/intldateformatter.format.php
    //      especially the examples
    // see: https://unicode-org.github.io/icu/userguide/format_parse/datetime/#formatting-dates
    'own_date_full_format'      => "EEEE, dd. MMMM yyyy - HH:mm" // "EEEE', the' dd. MMMM yyyy 'at' HH:mm 'o''clock' zz"
 	
);
