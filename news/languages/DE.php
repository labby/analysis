<?php

/**
 *  @module         news
 *  @version        see info.php of this module
 *  @author         Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos), LEPTON Project
 *  @copyright      2004-2010 Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos) 
 *  @copyright      2010-2022 LEPTON Project 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

//	Modul Description
$module_description = 'Mit diesem Modul k&ouml;nnen sie eine News Seite ihrer Seite hinzuf&uuml;gen.';

$MOD_NEWS = array(
    //	Variables for the backend
    'SETTINGS'          => 'News Einstellungen',
    'CONFIRM_DELETE'    => 'Sind Sie sicher, das Sie die News\nmit dem Titel &raquo;%s&laquo; \nl&ouml;schen m&ouml;chten?\nDas kann nicht widerufen werden!',
    //	Variables for the frontend
    'action'            => 'Aktion',
    'TEXT_READ_MORE'    => 'Weiterlesen',
    'TEXT_POSTED_BY'    => 'Veröffentlicht von',
    'TEXT_ON'           => 'am',
    'TEXT_LAST_CHANGED' => 'Zuletzt geändert am',
    'TEXT_AT'           => 'um',
    'TEXT_BACK'         => 'Zurück',
    'TEXT_COMMENTS'     => 'Kommentare',
    'TEXT_COMMENT'      => 'Kommentar',
    'TEXT_ADD_COMMENT'  => 'Kommentar hinzufügen',
    'TEXT_BY'           => 'von',
    'TEXT_PAGE_NOT_FOUND' => 'Seite nicht gefunden',
    'TEXT_UNKNOWN'      => 'Gast',
    'TEXT_NO_COMMENT'   => 'keine vorhanden',
    //	Variables for History
    'action'            => 'Aktion',
    'autosave_always'   => 'Änderungen veröffentlichen und bisherigen Inhalt in der Historie speichern (max. ' . MAX_WYSIWYG_HISTORY . ' möglich)',
    'button_copy'       => 'Entwürfe',
    'button_history'    => 'Historie',
    'comment'           => 'Kommentar',
    'date'              => 'Datum',
    'docs'              => 'Dokumentation',
    'header1'           => 'Nr',
    'publish_changes'   => 'Änderungen veröffentlichen und bisherigen Inhalt ersetzen (Standard)',
    'pushed_by'         => 'Zur Historie zugefügt von',
    'saved_by'          => 'Zuletzt gespeichert von',
    'save_ok'           => 'Daten erfolgreich gespeichert!',
    'use_workingcopy'   => 'Änderungen als Entwurf speichern (nur ein Entwurf pro Abschnitt möglich)',
    'version_delete'    => 'Löschen',
    'version_restore'   => 'Wiederherstellen',
    'version_view'      => 'Vorschau',
    'want_really'       => "wirklich löschen",
    'want_delete'       => "Wollen Sie die Version vom ",
    'want_delete_wc'    => "Wollen Sie den Entwurf von Sektion ",
    'wcopy_by'          => 'Als Entwurf gespeichert von',
    'what_to_do'        => 'Bitte wählen',
    // own date formats
    // see: https://www.php.net/manual/de/intldateformatter.format.php
    //      especially the examples
    // see: https://unicode-org.github.io/icu/userguide/format_parse/datetime/#formatting-dates
    'own_date_full_format' => "EEEE ', der' dd. MMMM yyyy 'um' HH:mm 'Uhr'"
);
