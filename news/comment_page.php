<?php

/**
 *  @module         news
 *  @version        see info.php of this module
 *  @author         Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos), LEPTON Project
 *  @copyright      2004-2010 Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos) 
 *  @copyright      2010-2022 LEPTON Project 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

require_once(LEPTON_PATH.'/modules/captcha_control/captcha/captcha.php');

$oNEWS = news::getInstance();

// Get comments page template details from db
$settings = array();
$database->execute_query(
	"SELECT `use_captcha`, `commenting` FROM `".TABLE_PREFIX."mod_news_settings` WHERE `section_id` = ".SECTION_ID ,
	true,
	$settings,
	false
);
if(count($settings) == 0)
{
	header("Location: ".LEPTON_URL.PAGES_DIRECTORY."");
	exit( 0 );
}
else
{
	// Print comments page
	$vars = array(
		'POST_TITLE'	=> POST_TITLE,
		'TEXT_COMMENT'	=> $oNEWS->language['TEXT_COMMENT']
	);
	
    $oTWIG = lib_twig_box::getInstance();	
	$oTWIG->registerModule("news");
	
	echo $oTWIG->render(
		'@news/comments_page.lte',
		$vars
	);
	
	$current_time=time(); 
	$_SESSION['submitted_when']=$current_time;
	
	$called_captcha = "";
	$recaptcha = "";
	if(file_exists(LEPTON_PATH."/modules/news/recaptcha.php")) {
		require_once LEPTON_PATH."/modules/news/recaptcha.php";
		$recaptcha = news_recaptcha::build_captcha();
	} else {
	
        /**
         *  internal captcha (old way)
         */
        ob_start();
            call_captcha( 
                captcha_control::getInstance()->pluginName,
                NULL,
                SECTION_ID
            );
            $called_captcha = ob_get_clean();
    }
	    
	/**
	 *	Here we go:
	 */
	$form_data = array(
		'LEPTON_URL'	=> LEPTON_URL,
		'SECTION_ID'	=> SECTION_ID,
		'PAGE_ID'	=> PAGE_ID,
		'POST_ID'	=> POST_ID,
		'ENABLED_ASP' => ( ENABLED_ASP ? 1 : 0 ),
		'TEXT'	=> $TEXT,
		'MOD_NEWS' => $oNEWS->language,
		'captcha_error' => isset($_SESSION['captcha_error']) ? 1 : 0,
		'captcha_error_message' => isset($_SESSION['captcha_error']) ? $_SESSION['captcha_error'] : "",
		'use_captcha'	=> $settings['use_captcha'],
		'call_captcha'	=> ($recaptcha != "" ? $recaptcha : $called_captcha),
		'comment_title'	=> isset($_SESSION['comment_title']) ? htmlspecialchars($_SESSION['comment_title']) : "",
		'comment_body'	=> isset($_SESSION['comment_body']) ? htmlspecialchars($_SESSION['comment_body']) : "",
		'leptoken'	=> isset($_GET['leptoken']) ? $_GET['leptoken'] : "",
		'date_w'	=> date('W'),
		'form_submitted_when' => $current_time 
	);
	
	echo $oTWIG->render(
		'@news/comments_form.lte',
		$form_data
	);

	if(isset($_SESSION['comment_title'])) unset($_SESSION['comment_title']);
	if(isset($_SESSION['comment_body'])) unset($_SESSION['comment_body']);
	if(isset($_SESSION['captcha_error'])) unset($_SESSION['captcha_error']);
	
}
