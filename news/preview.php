<?php

/**
 *  @module         news
 *  @version        see info.php of this module
 *  @author         Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos), LEPTON Project
 *  @copyright      2004-2010 Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos) 
 *  @copyright      2010-2022 LEPTON Project 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */
 
// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

if (isset($_GET['pid']))
{
    $iDisplayID = intval($_GET['pid']);
    if($iDisplayID < 1)
    {
        die( "[2] Nothing to display!" );
    }

    if( true === news_preview::getPreviewData( $iDisplayID ) )
    {
        $post_id = $iDisplayID;
        $page_id = news_preview::$aPreviewData["page_id"] ?? 0;
        $section_id = news_preview::$aPreviewData["section_id"] ?? 0;
        
        define("POST_ID", $iDisplayID);
        define("POST_SECTION", $section_id);
        
        require LEPTON_PATH."/index.php";
    
    } else {
    
        die("Can't get values for id ".$iDisplayID);
    }
}